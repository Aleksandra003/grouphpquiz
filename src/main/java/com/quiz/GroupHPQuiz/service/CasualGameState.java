package com.quiz.GroupHPQuiz.service;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
@Data
@Service
@RequiredArgsConstructor
public class CasualGameState extends GameState {
}
