package com.quiz.GroupHPQuiz.service;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
@SessionScope
@Data
@Service
@RequiredArgsConstructor
public abstract class GameState {
    private long points = 0;
    private int lastQuestion = 0;
    private boolean isActive = false;
    public List<Integer> indexOfQuestion = new ArrayList();


    public List<Integer> indexInRandomOrderOfQuestion(int firstIndex, int secondIndex) {
        List<Integer> indexes = new ArrayList<>();
        for (int i = firstIndex; i <= secondIndex; i++) {
            indexes.add(i);
        }
        Collections.shuffle(indexes);
        return indexes;
    }

}
