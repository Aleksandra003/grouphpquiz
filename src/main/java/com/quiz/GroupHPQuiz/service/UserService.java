package com.quiz.GroupHPQuiz.service;

import com.quiz.GroupHPQuiz.controller.model.UserRegistrationDto;

import com.quiz.GroupHPQuiz.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);
    User findByUserName(String userName);

    User save(UserRegistrationDto registration);

}
