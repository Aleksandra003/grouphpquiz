package com.quiz.GroupHPQuiz.service;

import com.google.gdata.util.io.base.UnicodeReader;
import com.quiz.GroupHPQuiz.model.*;
import com.quiz.GroupHPQuiz.model.repository.CasualQuizRepository;
import com.quiz.GroupHPQuiz.model.repository.MainQuizRepository;
import com.quiz.GroupHPQuiz.model.repository.QuizRepository;
import com.quiz.GroupHPQuiz.model.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;


@Component
@Service
@RequiredArgsConstructor
public class DBPopulatingService implements ApplicationListener<ApplicationReadyEvent> {
    private final BCryptPasswordEncoder passwordEncoder;
    private final CasualQuizRepository casualQuizRepository;
    private final MainQuizRepository mainQuizRepository;
    private final UserRepository userRepository;
    private String filepathCasualGame = "src/main/resources/questions/casualQuiz.csv";
    private String filepathMainGame = "src/main/resources/questions/mainQuiz.csv";
    String[] HEADERS = {"Question", "Option 1", "Option 2", "Option 3", "Option 4"};


    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        try {
            QuizReaderGame(filepathCasualGame, casualQuizRepository, CasualQuestion.class);
            QuizReaderGame(filepathMainGame, mainQuizRepository, MainQuestion.class);
            User user = new User();
            user.setUserName("archi");
            user.setFirstName("Arek");
            user.setEmail("bis211@wp.pl");
            user.setRoles(Arrays.asList(new Role("ROLE_ADMIN")));
            user.setPassword(passwordEncoder.encode("harry"));
            userRepository.save(user);
            User user2 = new User();
            user2.setUserName("olatutaj");
            user2.setFirstName("Ola");
            user2.setEmail("olatutaj@wp.pl");
            user2.setRoles(Arrays.asList(new Role("ROLE_USER")));
            user2.setPassword(passwordEncoder.encode("harry"));
            userRepository.save(user2);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public <T extends Question> void  QuizReaderGame(String filepath, QuizRepository<T> repository, Class<T> clasz) throws IOException {

        try (Reader in = new UnicodeReader(new FileInputStream(new File(filepath)), "UTF-8")) {
            Iterable<CSVRecord> records = CSVFormat.EXCEL
                    .withDelimiter(';')
                    .withHeader(HEADERS)
                    .withFirstRecordAsHeader()
                    .parse(in);
            for (CSVRecord record : records) {
                T question = clasz.getConstructor().newInstance();
                question.setQuestion(record.get("Question"));
                question.setReplyCorrect(record.get("Option 1"));
                question.setReplyWrong1(record.get("Option 2"));
                question.setReplyWrong2(record.get("Option 3"));
                question.setReplyWrong3(record.get("Option 4"));
                repository.save(question);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
