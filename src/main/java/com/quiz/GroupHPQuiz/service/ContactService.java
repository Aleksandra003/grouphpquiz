package com.quiz.GroupHPQuiz.service;

import com.quiz.GroupHPQuiz.controller.model.ContactForm;
import com.quiz.GroupHPQuiz.model.Contact;
import com.quiz.GroupHPQuiz.model.repository.ContactRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ContactService {
    private final ContactRepository contactRepository;

    public void addNew(Contact contact, Long id) {
        contactRepository.save(contact);

    }

    public void save(ContactForm contactForm) {
        Contact contact = new Contact();
        contact.setMail(contactForm.getMail());
        contact.setUserName(contactForm.getUserName());
        contact.setMessage(contactForm.getMessage());
        contactRepository.save(contact);

    }
}


