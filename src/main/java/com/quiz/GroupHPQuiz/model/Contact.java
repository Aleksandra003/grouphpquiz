package com.quiz.GroupHPQuiz.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@Entity
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, unique = true)
    @NotEmpty
    private String userName;
    @Column
    @NotEmpty
    @Email
    private String mail;
    @Column(nullable = false)
    @NotEmpty
    private String message;


}
