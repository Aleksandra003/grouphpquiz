package com.quiz.GroupHPQuiz.model.repository;

import com.quiz.GroupHPQuiz.model.CasualQuestion;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;

@Table(name="CasualQuiz")
@Repository
public interface CasualQuizRepository extends QuizRepository<CasualQuestion> {

CasualQuestion getById(Long id);

}
