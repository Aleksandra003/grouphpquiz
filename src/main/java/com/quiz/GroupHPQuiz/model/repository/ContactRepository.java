package com.quiz.GroupHPQuiz.model.repository;

import com.quiz.GroupHPQuiz.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
}
