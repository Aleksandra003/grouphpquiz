package com.quiz.GroupHPQuiz.model.repository;

import com.quiz.GroupHPQuiz.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface QuizRepository<T extends Question> extends JpaRepository<T, Long> {

}
