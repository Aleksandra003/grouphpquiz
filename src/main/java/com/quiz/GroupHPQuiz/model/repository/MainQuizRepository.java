package com.quiz.GroupHPQuiz.model.repository;

import com.quiz.GroupHPQuiz.model.MainQuestion;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;

@Table(name="MainQuiz")
@Repository
public interface MainQuizRepository extends QuizRepository<MainQuestion> {

    MainQuestion getById (Long id);

}
