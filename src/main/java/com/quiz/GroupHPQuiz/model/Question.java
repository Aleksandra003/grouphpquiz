package com.quiz.GroupHPQuiz.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@NoArgsConstructor
@Entity
@Data
public abstract class Question {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String question;
    @Column
    private String replyCorrect;
    @Column
    private String replyWrong1;
    @Column
    private String replyWrong2;
    @Column
    private String replyWrong3;

    public List<String> getAllAnswersInRandomOrder() {
        List <String> answers = new ArrayList<>();
        answers.add(replyCorrect);
        answers.add(replyWrong1);
        answers.add(replyWrong2);
        answers.add(replyWrong3);
        Collections.shuffle(answers);
        return answers;
    }
}
