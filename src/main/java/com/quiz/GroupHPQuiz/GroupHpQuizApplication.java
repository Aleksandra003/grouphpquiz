package com.quiz.GroupHPQuiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroupHpQuizApplication {

	public static void main(String[] args) {
		SpringApplication.run(GroupHpQuizApplication.class, args);

	}

}
