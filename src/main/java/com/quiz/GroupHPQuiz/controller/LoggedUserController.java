package com.quiz.GroupHPQuiz.controller;

import com.quiz.GroupHPQuiz.model.User;
import com.quiz.GroupHPQuiz.service.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@AllArgsConstructor

public class LoggedUserController {


    private final UserServiceImpl userService;

    @GetMapping("/userName")
    public String currentUserName (Authentication authentication){
        return authentication.getName();
    }

    @GetMapping("user/{userName}")
    public String getLoggedUser(Model model, @PathVariable String userName) {
        User user = userService.findByUserName(userName);
        model.addAttribute("userName", userName);
        return "user";
    }
}
