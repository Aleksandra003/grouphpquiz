package com.quiz.GroupHPQuiz.controller;

import com.quiz.GroupHPQuiz.model.MainQuestion;
import com.quiz.GroupHPQuiz.model.repository.MainQuizRepository;
import com.quiz.GroupHPQuiz.service.MainGameState;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequiredArgsConstructor
public class MainQuizController {
    private final MainQuizRepository mainQuizRepository;
    private final MainGameState mainGameState;


    @GetMapping("/mainQuiz")
    public String quiz(Model model) {
        int val = mainGameState.getLastQuestion();
        if (mainGameState.isActive() == false) {
            mainGameState.setActive(true);
            mainGameState.setIndexOfQuestion(mainGameState.indexInRandomOrderOfQuestion(6, (int) mainQuizRepository.count()).subList(0, 10));
            System.out.println(mainGameState.getIndexOfQuestion());
            int indexQuestion = mainGameState.getIndexOfQuestion().get(val);
            MainQuestion mainQuestion = mainQuizRepository.findById((long) indexQuestion).get();
            model.addAttribute("mainQuestion", mainQuestion);

        } else {
            int indexQuestion = mainGameState.getIndexOfQuestion().get(val);
            MainQuestion mainQuestion = mainQuizRepository.findById((long) indexQuestion).get();
            model.addAttribute("mainQuestion", mainQuestion);
        }
        model.addAttribute("indexQuestion", mainGameState);
        model.addAttribute("userAnswer", new QuizFormData());
        return "mainQuiz";
    }


    @RequestMapping(value = "/saveMainQuiz", method = RequestMethod.POST)
    public String saveQuiz(@ModelAttribute QuizFormData userAnswer, BindingResult errors, Model model) {
        int val = mainGameState.getLastQuestion();
        int indexQuestion = mainGameState.getIndexOfQuestion().get(val);
        MainQuestion casualQuestion = mainQuizRepository.findById((long) indexQuestion).get();
        if (userAnswer.getUserAnswer().equals(casualQuestion.getReplyCorrect())) {
            model.addAttribute("tresc", "Dobrze");
            mainGameState.setPoints(mainGameState.getPoints() + 1);
        } else {
            model.addAttribute("tresc", "Zle!");
        }
        if (val < mainGameState.getIndexOfQuestion().size() - 1) {
            mainGameState.setLastQuestion(val + 1);
            return "redirect:/mainQuiz";
        } else {
            return "redirect:/mainQuizResults";
        }
    }


    @GetMapping("/mainQuizResults")
    public String mainQuizMResults(Model model) {
        model.addAttribute("tresc", mainGameState.getPoints());
        mainGameState.setPoints(0);
        mainGameState.setLastQuestion(0);
        mainGameState.setActive(false);
        return "mainQuizResults";
    }

    @RequestMapping(value = "/mainQuizReset", method = RequestMethod.GET)
    public String resetMainQuiz(Model model) {
        return ("redirect:/mainQuiz");
    }
}