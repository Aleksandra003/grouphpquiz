package com.quiz.GroupHPQuiz.controller;

import com.quiz.GroupHPQuiz.controller.model.UserRegistrationDto;
import com.quiz.GroupHPQuiz.model.User;
import com.quiz.GroupHPQuiz.service.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("registration")
@AllArgsConstructor
public class UserRegistrationController {

    private UserServiceImpl userServiceImpl;

    @ModelAttribute("user")
    public UserRegistrationDto userRegistrationDto() {
        return new UserRegistrationDto();
    }

    @GetMapping
    public String showRegistrationForm(Model model) {
        return "user/registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") @Valid UserRegistrationDto userDto,
                                      BindingResult result) {

        User existing = userServiceImpl.findByEmail(userDto.getEmail());
        if (existing != null) {
            result.rejectValue("email", null, "Konto o podanym adresie e-mail istnieje.");
        }


        if (result.hasErrors()) {
            return "user/registration";
        }

        User existingUserName = userServiceImpl.findByUserName(userDto.getUserName());
        if (existingUserName != null) {
            result.rejectValue("userName", null, "Konto o podanej nazwie użytkownika istnieje.");
        }


        if (result.hasErrors()) {
            return "user/registration";
        }


        userServiceImpl.save(userDto);
        return "redirect:/registration?success";
    }
}
