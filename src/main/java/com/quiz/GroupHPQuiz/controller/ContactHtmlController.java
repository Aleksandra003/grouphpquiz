package com.quiz.GroupHPQuiz.controller;

import com.quiz.GroupHPQuiz.controller.model.ContactForm;
import com.quiz.GroupHPQuiz.service.ContactService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
@AllArgsConstructor
public class ContactHtmlController {
    private final ContactService contactService;

    @GetMapping("/html/contact")
    public String addContact(Model model) {
        model.addAttribute("contactForm", new ContactForm());
        return "/contact";
    }

    @PostMapping("/html/contact")
    public String addContact(@Valid ContactForm contactForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("contactForm", contactForm);
            return "/contact";
        }
        contactService.save(contactForm);
        return "index";
    }
}

