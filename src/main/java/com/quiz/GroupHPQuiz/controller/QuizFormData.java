package com.quiz.GroupHPQuiz.controller;

import lombok.Data;

@Data
public class QuizFormData {
    private String userAnswer;
}

