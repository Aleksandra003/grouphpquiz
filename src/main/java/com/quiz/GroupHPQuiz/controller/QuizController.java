package com.quiz.GroupHPQuiz.controller;

import com.quiz.GroupHPQuiz.model.CasualQuestion;
import com.quiz.GroupHPQuiz.model.repository.CasualQuizRepository;
import com.quiz.GroupHPQuiz.service.CasualGameState;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequiredArgsConstructor
public class QuizController {
    private final CasualQuizRepository casualQuizRepository;
    private final CasualGameState casualGameState;


    @GetMapping("/quiz")
    public String quiz(Model model) {
        int val = casualGameState.getLastQuestion();
        if (casualGameState.isActive() == false) {
            casualGameState.setActive(true);
            casualGameState.setIndexOfQuestion(casualGameState.indexInRandomOrderOfQuestion(1,5));
            int indexQuestion = casualGameState.getIndexOfQuestion().get(val);
            CasualQuestion casualQuestion = casualQuizRepository.findById((long) indexQuestion).get();
            model.addAttribute("casualQuestion", casualQuestion);

        } else {
            int indexQuestion = casualGameState.getIndexOfQuestion().get(val);
            CasualQuestion casualQuestion = casualQuizRepository.findById((long) indexQuestion).get();
            model.addAttribute("casualQuestion", casualQuestion);
        }
        model.addAttribute("indexQuestion", casualGameState);
        model.addAttribute("userAnswer", new QuizFormData());
        return "quiz";
    }


    @RequestMapping(value = "/saveQuiz", method = RequestMethod.POST)
    public String saveQuiz(@ModelAttribute QuizFormData userAnswer, BindingResult errors, Model model) {
        int val = casualGameState.getLastQuestion();
        int indexQuestion = casualGameState.getIndexOfQuestion().get(val);
        CasualQuestion casualQuestion = casualQuizRepository.findById((long) indexQuestion).get();
        if (userAnswer.getUserAnswer().equals(casualQuestion.getReplyCorrect())) {
            model.addAttribute("tresc", "Dobrze");
            casualGameState.setPoints(casualGameState.getPoints() + 1);
        } else {
            model.addAttribute("tresc", "Zle!");
        }
        if (val < casualGameState.getIndexOfQuestion().size() - 1) {
            casualGameState.setLastQuestion(val + 1);
            return "redirect:/quiz";
        } else {
            return "redirect:/quizResults";
        }
    }

    @GetMapping("/quizResults")
    public String quizResults(Model model) {
        model.addAttribute("tresc", casualGameState.getPoints());
        casualGameState.setPoints(0);
        casualGameState.setLastQuestion(0);
        casualGameState.setActive(false);
        return "quizResults";
    }

    @RequestMapping(value = "/quizReset", method = RequestMethod.GET)
    public String resetQuiz(Model model) {
        return ("redirect:/quiz");
    }
}
