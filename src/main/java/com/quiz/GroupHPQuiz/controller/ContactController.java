package com.quiz.GroupHPQuiz.controller;

import com.quiz.GroupHPQuiz.model.Contact;
import com.quiz.GroupHPQuiz.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
public class ContactController {
    private final ContactService contactService;

    @PostMapping("/contact")
    public void addContact(@RequestBody Contact contact, @RequestParam(name = "id") Long id) {
        contactService.addNew(contact, id);
    }
}

