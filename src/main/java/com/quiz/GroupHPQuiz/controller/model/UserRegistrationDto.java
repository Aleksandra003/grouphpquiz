package com.quiz.GroupHPQuiz.controller.model;


import com.quiz.GroupHPQuiz.constraint.FieldMatch;
import lombok.Data;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@FieldMatch.List({
        @FieldMatch(first = "password", second = "confirmPassword", message = "Hasła muszą być takie same"),
        @FieldMatch(first = "email", second = "confirmEmail", message = "Adresy e-mail musze się zgadzać")
})


@Data
public class UserRegistrationDto {
    @NotEmpty
    private String firstName;

    private String lastName;

    @NotEmpty
    private String password;

    @NotEmpty
    private String confirmPassword;

    @NotEmpty
    private String userName;


    @Email
    @NotEmpty
    private String email;

    @Email
    @NotEmpty
    private String confirmEmail;

    @AssertTrue
    private Boolean terms;

}
