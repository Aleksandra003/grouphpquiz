package com.quiz.GroupHPQuiz.controller.model;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class ContactForm {

    @NotEmpty
    private String userName;
    @Email
    @NotEmpty
    private String mail;
    @NotEmpty
    private String message;
}
